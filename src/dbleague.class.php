<?php 
include_once 'db.class.php';

class dbleague
{
	
	static function loadLeague($leagueid) {
		global $db;
		$query = $db->prepare("select * from league where leagueid = :leagueid ");
		$query->execute(array('leagueid' => $leagueid));
		$league = $query->fetch(PDO::FETCH_OBJ);				
		return $league;	
	}
	
	static function loadUsersleagues($userid) {
		global $db;
		$query = $db->prepare("select * from userleague,league where userleague.userid = :userid and league.leagueid = userleague.leagueid order by league.name");
		$query->execute(array('userid' => $userid));
		$results = $query->fetchAll(PDO::FETCH_OBJ);	
		
		return $results;
	}
	
	static function loadUserLeague($userid, $leagueid) {
		global $db;
		$query = $db->prepare("select league.name from userleague, league where userleague.userid = :userid and userleague.leagueid = :leagueid and league.leagueid = userleague.leagueid");
		$query->execute(array('userid' => $userid, 'leagueid' => $leagueid));
		$result = $query->fetch(PDO::FETCH_OBJ);	
		
		return $result;
	}
	
	static function loadLeagueStandings($leagueid) {
		global $db;
		$query = $db->prepare("select user.name, user.totalpoints from user, userleague where userleague.leagueid = :leagueid and user.userid = userleague.userid");
		$query->execute(array('leagueid' => $leagueid));
		$results = $query->fetchAll(PDO::FETCH_OBJ);	
		
		return $results;
	}
			
	static function createleague($userid, $name) {
		global $db;
		$sth = $db->prepare("insert into league (name) values (:name)");
		$data = array("name" => $name);
		$sth->execute($data);
		$leagueid = $db->lastInsertId();
		
		$code = md5($name . $leagueid . date("dmYhis"));
		$sth = $db->prepare("update league set code= :code where leagueid = :leagueid ");
		$sth->execute(array("code" => $code, "leagueid" => $leagueid));
		return $code;		
	}
	
	static function joinleague($userid,$code) {
		global $db;
		$query = $db->prepare("select leagueid from league where code = :code");
		$leagueid = 0;		
		$error = "";
		$result = $query->execute(array("code" => $code));
		$leagueid = $result['leagueid'];
		if($leagueid == 0) {
			$error = "no league found";
		}		
		else {
			league::createUserleague($userid,$leagueid);
		}		
		return $error;		
	}	
	
	static function updateleague($leagueid,$name,$password) {
		global $db;
		$query = $db->prepare("update league set name=:name, password=:password where leagueid=:leagueid");
		$query->execute(array("name" => $name, "password" => $password, "leagueid" => $leagueid));					
	}	
		
	static function createUserleague($userid, $leagueid) {
		global $db;
		$query = $db->prepare("insert into userleague (userid,leagueid) values (:userid,:leagueid)");
		$query->execute(array("userid" => $userid, "leagueid" => $leagueid));
	}
	
	static function deleteleague($leagueid) {
		global $db;
		$query = $db->prepare("delete from league where leagueid = :leagueid");
		$query->execute(array("leagueid" => $leagueid));
	}
	
	static function deleteUserleague($userid, $leagueid) {
		global $db;
		$query = $db->prepare("delete from userleague where userid = :userid and leagueid = :leagueid");
		$query->execute(array("leagueid" => $leagueid, "userid" => $userid));
		$query = $db->prepare("select count(userid) from userleague where leagueid = :leagueid");
		$result = $query->execute(array("leagueid" => $leagueid));
		$numUsers = $result[0];
		if($numUsers == 0){
			league::deleteleague($leagueid);
		}
	}
	
	/** select statement to load all the users for all the leagues that you are associated with - might be useful later! 
	select leaguelist.name as leaguename, user.name as username, user.totalpoints from (select league.name, league.leagueid from league,userleague where league.leagueid = userleague.leagueid and userleague.userid = 11) as leaguelist inner join userleague on leaguelist.leagueid = userleague.leagueid  inner join user on user.userid = userleague.userid order by leaguelist.name, user.totalpoints desc **/
	
}

