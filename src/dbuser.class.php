<?php 
include_once 'db.class.php';

class dbuser
{
	
	static function loadUser($email, $password){
		global $db;
		$query = $db->prepare("select * from user where email = :email and password = :password");
		$query->execute(array('email' => $email, 'password' => $password));
		$user  = $query->fetch(PDO::FETCH_OBJ);
		return $user;		
	}
	
	static function checkUser($email){
		global $db;
		$query = $db->prepare("select * from user where email = :email");
		$query->execute(array('email' => $email));
		$user  = $query->fetch(PDO::FETCH_OBJ);	
		return $user;		
	}
	
	static function loadUserID($userid){
		global $db;
		$query = $db->prepare("select * from user where userid = :userid");
		$query->execute(array('userid' => $userid));
		$user  = $query->fetch(PDO::FETCH_OBJ);	
		return $user;				
	}
	/**
	static function loadUserList($username = "",$firstname = "",$lastname = "",$email = "") {
		global $db;
		$query = "select * from user";
		if($firstname != ""){
			$query = $query + " and firstname = :firstname";
		}
		if($lastname != ""){
			$query = $query + " and lastname = :lastname";
		}
		if($email != ""){
			$query = $query + " and email = :email";
		}
		$wordCount = str_word_count($query,0,"where");
		if ($wordCount) {
			$finalQuery = str_replace_once("and","where",$query);
		}
		    $results = $db->get_results($finalQuery);
		
		return $results;
	}
	**/
	static function createUser($email,$password,$name){
		global $db;
		$sth = $db->prepare("insert into user (password,email,name) value (:password,:email,:name)");
		$data = array($email, $password, $name);
		$sth->execute($data);
		$userid = $db->lastInsertId();
		return $userid;
	}
	
	static function saveUser($userid,$username,$password,$firstname,$lastname,$email,$active,$correspond) {
		global $db;
		$query = "update user set username = '" . $username . 
				 "', firstname = '" . $firstname .
				 "', lastname  = '" . $lastname .
				 "', email = '" . $email .
				 "', active = '" . $active .
				 "', correspond = '" . $correspond .
				 "', where iduser = " . $userid;
		$success = $db->query($query);
			
		return $success;
	}
	
	static function makeInactive($userid) {
		global $db;
		$query = "update user set active=0 where iduser = " . $userid;
		$success = $db->query($query);
		
		return $success;
	}
	
	private function str_replace_once($str_pattern, $str_replacement, $string){	
		if (strpos($string, $str_pattern) !== false){
			$occurrence = strpos($string, $str_pattern);
			return substr_replace($string, $str_replacement, strpos($string, $str_pattern), strlen($str_pattern));
		}
	   
		return $string;
    } 
}
?>