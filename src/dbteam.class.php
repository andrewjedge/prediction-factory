<?php 
include_once 'db.class.php';

class dbteam {

	static function loadTeamsByCompetition($compid) {
		global $db;
		$query = $db->prepare("select teamid, name from team where competitionid = :compid order by name");
		$query->execute(array('compid' => $compid));
		$results = $query->fetchAll(PDO::FETCH_OBJ);		
		return $results;
	}
	
}
