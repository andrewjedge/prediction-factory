<!DOCTYPE html> 
<html> 
<head> 
	<title>Prediction Factory</title> 
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.0-beta.1/jquery.mobile-1.3.0-beta.1.min.css" />
	<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.3.0-beta.1/jquery.mobile-1.3.0-beta.1.min.js"></script>
	<script src="/js/prediction.js"></script>
	<link rel="stylesheet" href="/css/prediction.css" />

<?php
include_once 'session.php';
include_once 'dbleague.class.php';

$leagues = dbleague::loadUsersLeagues($user->userid);
?>
</head>
<body>
<div data-role="page">

	<div data-role="header">
		<h1>Prediction Factory</h1>
	</div><!-- /header -->

	<div data-role="content">
		<div class="ui-grid-a league-breakpoint">
			<div class="ui-block-a ">
				<div class="content-secondary">
					<ul data-role="listview" data-inset="true">
						<li data-role="list-divider">Main Menu</li>
						<li><a href="/prediction/home.php">Home</a></li>
						<li><a href="/prediction/leagues.php">Leagues</a></li>
						<li><a href="/prediction/matches.php">Predictions</a></li>				
					</ul>
				</div>
			</div>
			<div class="ui-block-b ">
				<div class="content-primary">
				<?php 
				if(isset($leagues)){ 
					echo '<ul data-role="listview" data-inset="true">';
					echo '<li data-role="list-divider">Your Leagues</li>';
					foreach($leagues as $league){ 
						echo '<li><a href="standings.php?lid='. $league->leagueid .'">'. $league->name .'</a></li>';
					}
					echo '</ul>';
				}	
				?>		
				</div>
			</div>
		</div>
	</div>

</div>
</body>
</html>
