<!DOCTYPE html> 
<html> 
<head> 
	<title>Prediction Factory</title> 
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.0-beta.1/jquery.mobile-1.3.0-beta.1.min.css" />
	<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.3.0-beta.1/jquery.mobile-1.3.0-beta.1.min.js"></script>
	<script src="/js/prediction.js"></script>
	<link rel="stylesheet" href="/css/prediction.css" />
	
<?php
include_once 'session.php';
include_once 'dbgame.class.php';
include_once 'dbteam.class.php';

$todaysDate  = date("Y-m-d");
$todaysDateTime = strtotime($todaysDate);
$user = $_SESSION['predictionuser'];
$competition = dbgame::loadActiveCompetition();
$games = null;
$action = "";

if($action == "results" || $action == "predictions"){
	$gameIdList  = $_POST['gameIdList'];
	$team1Scores = $_POST['team1ScoreList'];
	$team2Scores = $_POST['team2ScoreList'];
	$starGames   = $_POST['starList'];
	$heartGames   = $_POST['heartList'];
		
	if(($action == "predictions") && ($gameIdList != null)){
		dbgame::createPredictions($user->userid,$gameIdList,$team1Scores,$team2Scores,$starGames,$heartGames);
	}
	
	if(($action == "results") && ($gameIdList != null)){
		dbgame::enterResults($gameIdList,$team1Scores,$team2Scores);
	}
}
if($action == "setup"){
	if ($user->accesslevel < 10){
		echo '<div class="noAccess divPageContent"> You do not have access to this page </div>';
	} else {
		
		if($split != ""){
			if($competition->datasplit == "group"){
				$games = dbgame::loadGamesByCompetitionGroup($competition->competitionid, $split);
			} else {
				$games = dbgame::loadGamesByCompetitionWeek($competition->competitionid, $split);
			}
		} 
	}
} else if ($action == "predictions") {
	$games = dbgame::loadPredsByCompetition($competition->competitionid, $userid);
}
$teams = dbteam::loadTeamsByCompetition($competition->competitionid);
if($games == null){
	$games = dbgame::loadGamesByCompetition($competition->competitionid, $competition->datasplit);
}
?>
</head>
<body>
<div data-role="page">

	<div data-role="header">
		<h1>Prediction Factory</h1>
	</div><!-- /header -->
	
	<div data-role="content">
		<form id="gameForm" method="post" action="<?php $_SERVER['PHP_SELF'] ?>" >
			<table data-role="table" id="tblMatches" data-mode="reflow" class="ui-responsive table-stroke" style="display:table;" >
				<thead>
					<tr>
						<th>Date</th>
						<th>Group</th>
						<th>Game</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php 
					if(isset($games)){
						$currentGroup = "";
						$gameIdList = "";
						$starList = "";
						$heartList = "";							
						foreach($games as $game) {
						
							echo '<tr>';
							echo '<td>'. $game->date .'</td>';
							echo '<td>'. $game->group .'</td>';
							echo '<td>';
							echo '<div class="ui-grid-b">';
							echo '<div class="ui-block-a">';
							echo '<h3>'. $game->t1name .'</h3>';
							echo '</div><div class="ui-block-b">';
							echo '<select name="result'. $game->gameid .'" >';
							echo '<option value="0|0">0-0</option>';
							echo '<option value="1|0">1-0</option>';
							echo '<option value="0|1">0-1</option>';
							echo '<option value="1|1">1-1</option>';
							echo '<option value="2|0">2-0</option>';
							echo '<option value="0|2">0-2</option>';							
							echo '</select>';
							echo '</div><div class="ui-block-c">';
							echo '<h3>'. $game->t2name .'</h3>';
							echo '</div></div></td>';
							echo '<td></td>';
							echo '</tr>';							
						}
					
					} ?>
				</tbody>
			</table>
			<input type="hidden" id="hdnTeam1ScoreList" name="team1ScoreList" value="" />
			<input type="hidden" id="hdnTeam2ScoreList" name="team2ScoreList" value="" />
			<input type="hidden" id="hdnHeartList" name="heartList" value="<?php echo $heartList ?>" />
			<input type="hidden" id="hdnStarList" name="starList" value="<?php echo $starList ?>" />
			<input type="hidden" id="hdnAction" name="action" value="<?php echo $action ?>" />
			<input type="hidden" id="hdnGameIdList" name="gameIdList" value="<?php echo $gameIdList ?>" />
		</form>
	</div>		
</div>
</body>
</html>
