<!DOCTYPE html> 
<html> 
<head> 
	<title>Prediction Factory</title> 
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.0-beta.1/jquery.mobile-1.3.0-beta.1.min.css" />
	<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.3.0-beta.1/jquery.mobile-1.3.0-beta.1.min.js"></script>
	<script src="/js/prediction.js"></script>
	<link rel="stylesheet" href="/css/prediction.css" />
<?php 
include_once 'dbuser.class.php';
$error = "";

if(isset($_POST) && array_key_exists('login',$_POST)){
	$email    = $_POST['email'];
	$password = md5($_POST['password']);
	
	$user = dbuser::loadUser($email, $password);
		
	if($user != null) {
		session_start();
		$_SESSION['predictionuser'] = $user;
		header("location:leagues.php");
		exit;
	}
	else {
		$error = "Incorrect Email or Password";
	}
}	
?>
</head> 
<body> 

<div data-role="page">

	<div data-role="header">
		<h1>Prediction Factory</h1>
	</div><!-- /header -->
	<div data-role="content">
		<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" data-ajax="false">
			<?php if($error != "") {
				echo '<div class="errorBox"><p>'. $error . '</p></div>';
			} ?>
			<div data-role="field-contain" class="ui-hide-label">
				<label for="txtEmail" >Email</label> 
				<input id="txtEmail" name="email" type="text" placeholder="Email" />
			</div>
			<div data-role="field-contain" class="ui-hide-label">
				<label for="txtPassword" >Password</label>
				<input id="txtPassword" name="password" type="password" placeholder="Password" />
			</div>			
			<div><button type="submit" data-theme="b" name="login" value="login" >Login</button></div>
						
			<div><button type="button" >Register</button></div>			
		</form>
	</div><!-- /content -->
	
</div><!-- /page -->

</body>
</html>
