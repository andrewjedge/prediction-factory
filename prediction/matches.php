<!DOCTYPE html> 
<html> 
<head> 
	<title>Prediction Factory</title> 
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.0-beta.1/jquery.mobile-1.3.0-beta.1.min.css" />
	<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.3.0-beta.1/jquery.mobile-1.3.0-beta.1.min.js"></script>
	<script src="/js/prediction.js"></script>
	<link rel="stylesheet" href="/css/prediction.css" />
	
<?php
include_once 'session.php';
include_once 'dbgame.class.php';
include_once 'dbteam.class.php';
include_once 'dbparam.class.php';

$todaysDate  = date("Y-m-d");
$todaysDateTime = strtotime($todaysDate);
$user = $_SESSION['predictionuser'];
$competition = dbgame::loadActiveCompetition();
$games = null;
$action = "";

if($action == "results" || $action == "predictions"){
	$gameIdList  = $_POST['gameIdList'];
	$team1Scores = $_POST['team1ScoreList'];
	$team2Scores = $_POST['team2ScoreList'];
	$starGames   = $_POST['starList'];
	$heartGames   = $_POST['heartList'];
		
	if(($action == "predictions") && ($gameIdList != null)){
		dbgame::createPredictions($user->userid,$gameIdList,$team1Scores,$team2Scores,$starGames,$heartGames);
	}
	
	if(($action == "results") && ($gameIdList != null)){
		dbgame::enterResults($gameIdList,$team1Scores,$team2Scores);
	}
}
if($action == "setup"){
	if ($user->accesslevel < 10){
		echo '<div class="noAccess divPageContent"> You do not have access to this page </div>';
	} else {
		
		if($split != ""){
			if($competition->datasplit == "group"){
				$games = dbgame::loadGamesByCompetitionGroup($competition->competitionid, $split);
			} else {
				$games = dbgame::loadGamesByCompetitionWeek($competition->competitionid, $split);
			}
		} 
	}
} else if ($action == "predictions") {
	$games = dbgame::loadPredsByCompetition($competition->competitionid, $userid);
}
$teams = dbteam::loadTeamsByCompetition($competition->competitionid);
if($games == null){
	$games = dbgame::loadGamesByCompetition($competition->competitionid, $competition->datasplit);
}
?>

<script>
function selectMultiplier(gameid, type){
	
	var newGameID = newID.substring(newID.indexOf('-') + 1); 
	var currentMultiplier = $("#currentMultiplier-" + gameid).val();
	var numHearts = $("#hdnNumHearts").val();
	var numStars = $("#hdnNumStars").val();
	
	if(type == currentMultiplier){
		return;
	}
	
	var error = false;
	
	if(type == "heart"){
		
		if(numHearts >= 1){
			alert("You can only select one x3 game");
			error = true;
		} else {
			numHearts++;
			currentMultiplier = "heart";
		}	
	}
	else if(type == "star"){
		
		if(numStars >= 3){
			alert("You can only select three x2 games");
			error = true;
		} else {
			numStars++;
			currentMultiplier = "star";
		}	
	}
	
	switch(currentMultiplier){
		case 'heart':
			if(error){
				$("#radio-heart-" + gameid).checked(true);
			} else {
				numHearts--;				
			}
		case 'star':
			if(error){
				$("#radio-star-" + gameid).checked(true);
			} else {
				numStars--;				
			}
		case 'none':
			if(error){
				$("#radio-none-" + gameid).checked(true);
			}
	}
	
	$("#currentMultiplier-" + gameid).val(currentMultiplier);
	$("#hdnNumStars").val(numStars);
	$("#hdnNumHearts").val(numHearts);
}
</script>

</head>
<body>
<div data-role="page">

	<div data-role="header">
		<h1>Prediction Factory</h1>
	</div><!-- /header -->
	
	<div data-role="content">
		<form id="gameForm" method="post" action="<?php $_SERVER['PHP_SELF'] ?>" >
			<ul data-role="listview" data-inset="true">
					<?php 
					if(isset($games)){
						$scoreoptions = dbparam::loadParametersBySection("scoreoption");
						
						$currentGroup = "";
						$gameIdList = "";
						$starList = "";
						$heartList = "";		
						$numHearts = 0;
						$numStars  = 0;
						foreach($games as $game) {
							$starCheck = "";
							$heartCheck = "";
							$noneCheck = "";
							$currentMultiplier = "";
														
							if($currentGroup != $game->group){
								echo '<li data-role="list-divider">Group '. $game->group .'</li>';
								$currentGroup = $game->group;
							}
							
							if(isset($game->multiplier) && ($game->multiplier == "Star")){
								$starCheck = "checked";
								$currentMultiplier = "star";
								$numStars++;
							}
							else if(isset($game->multiplier) && ($game->multiplier == "Heart")){
								$heartCheck = "checked";
								$currentMultiplier = "heart";
								$numHearts++;
							} else {								
								$noneCheck = "checked";
								$currentMultiplier = "none";
							}
							
					?>
							
					<li>
						<div class="ui-grid-b matches-grid">
							<div class="ui-block-a">
								<h3> <?php echo $game->t1name .' v '. $game->t2name ?></h3>
							</div>
							<div class="ui-block-b">
									<?php
									if(isset($scoreoptions)){
										echo '<select name="result'. $game->gameid .'" >';
										foreach($scoreoptions as $option){
											echo '<option value="'. $option->key .'">'. $option->value .'</option>';
										}
										echo '</select>';
									}								
									?>
							</div>
							<div class="ui-block-c">
									<fieldset data-role="controlgroup" data-type="horizontal">
										<input type="radio" name="multiplier-<?php echo $game->gameid ?>" id="radio-heart-<?php echo $game->gameid ?>" value="heart" <?php echo $heartCheck; ?> onclick="selectMultiplier('<?php echo $game->gameid ?>','heart');" />
										<label for="radio-heart-<?php echo $game->gameid ?>">x2</label>
										<input type="radio" name="multiplier-<?php echo $game->gameid ?>" id="radio-star-<?php echo $game->gameid ?>" value="Star" <?php echo $starCheck;  ?> onclick="selectMultiplier('<?php echo $game->gameid ?>','star');" />
										<label for="radio-star-<?php echo $game->gameid ?>">x3</label>
										<input type="radio" name="multiplier-<?php echo $game->gameid ?>" id="radio-none-<?php echo $game->gameid ?>" value="None" <?php echo $noneCheck; ?>  onclick="selectMultiplier('<?php echo $game->gameid ?>','none');" />
										<label for="radio-none-<?php echo $game->gameid ?>">-</label>
										<input type="hidden" id="currentMultiplier-<?php echo $game->gameid ?>" value="<? echo $currentMultiplier; ?>" /> 
									</fieldset>
							</div>
						</div>
					</li>					
					<?php
						}
					
					} 
					?>
			</ul>
			<input type="hidden" id="hdnNumHearts" name="numHearts" value="<?php echo $numHearts ?>" />
			<input type="hidden" id="hdnNumStars" name="numStars" value="<?php echo $numStars ?>" />
			<input type="hidden" id="hdnAction" name="action" value="<?php echo $action ?>" />
			<input type="hidden" id="hdnGameIdList" name="gameIdList" value="<?php echo $gameIdList ?>" />
		</form>
	</div>		
</div>
</body>
</html>
